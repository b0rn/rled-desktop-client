import pyaudio
import socket
import numpy as np
from time import sleep
from threading import Thread
import pickle

from ctypes import *

ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
def py_error_handler(filename, line, function, err, fmt):
  pass
c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)

asound = cdll.LoadLibrary('libasound.so')
# Set error handler
asound.snd_lib_error_set_handler(c_error_handler)

class AudioVisualizer(Thread):
    def __init__(self,args):
        Thread.__init__(self)
        self.host = args["HOST"]
        self.port = args["PORT"]
        self.refresh_rate = args["REFRESH_RATE"]
        self.channel_count = args["CHANNEL_COUNT"]
        self.sample_rate = args["SAMPLE_RATE"]
        self.sample_max = args["SAMPLE_MAX"]
        self.buffer_size = args["BUFFER_SIZE"]
        self.led_count = args["LED_COUNT"]
        self.colors = [np.array(color) for color in args["COLORS"]]
        self.py = pyaudio.PyAudio()
        self.stream = None
        self.running = False
        self.errorCallback = None

    def run(self):
        self.stream = self.py.open(format = pyaudio.paInt16,
                        channels = self.channel_count,
                        rate = self.sample_rate,
                        input = True,
                        frames_per_buffer = self.buffer_size)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.running = True

        while self.running:
            try:
                data = self.get_data()
                if data is not None:
                    payload = pickle.dumps(data)
                    sock.sendto(payload,(self.host,self.port))
                sleep(self.refresh_rate / 1000)
            except Exception as e:
                self.errorCallback(e)
                self.stream.close()
        self.stream.close()

    def stop(self):
        self.running = False

    def setErrorCallback(self,f):
        self.errorCallback = f

    def get_data(self):
        if not self.stream:
            return

        data = np.fromstring(self.stream.read(self.stream.get_read_available()), 'int16').astype(float)
        getColor = self.gen_color_func()

        if len(data):
            fft = np.absolute(np.fft.rfft(data, n=len(data)))
            colors = [None for i in range(self.led_count)]
            step = int(len(fft) / len(colors))

            for i in range(len(colors)):
                x = np.mean(fft[i:i+step])
                x = x / self.sample_max
                color = getColor(x)
                colors[i] = color.astype(int).tolist()

            return colors

    def gen_color_func(self):
        t = np.linspace(0,1,len(self.colors))

        def getColor(x):
            inf = 0
            sup = None
            for i in range(len(t)):
                if x < t[i]:
                    sup = i
                    break
                inf = i
            if sup is None:
                return self.colors[inf]
            a = 1.0 / (t[sup] - t[inf])
            b = (-t[inf])/(t[sup]-t[inf])
            factor = a*x+b
            return (self.colors[sup] - self.colors[inf]) * factor + self.colors[inf]
        return getColor
