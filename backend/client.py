import socket
import json

class Client:
    def __init__(self):
        self.controller = None
        self.currentApp = None
        self.socket = None
        self.connected = False
        self.appErrorCallback = None
        self.notConnectedErrorCallback = None

    def setController(self,controller):
        self.controller = controller

    def connect(self,host,port):
        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.socket.connect((host,port))
        self.connected = True

    def disconnect(self):
        self.connected = False
        if self.socket is not None:
            self.socket.close()
            self.socket = None

    def isConnected(self):
        return self.connected

    def startApp(self,app,args):
        if self.currentApp is not None:
            self.stopApp()

        try:
            # Send payload
            self.socket.sendall(bytes(json.dumps(args),"utf-8"))
        except:
            self.notConnectedErrorCallback()
            return False

        # Wait for response
        recv = self.socket.recv(1024)
        if not recv:
            self.notConnectedErrorCallback()
            return False

        resp = json.loads(recv)

        if resp["APP_LAUNCHED"] is not True:
            raise Exception(resp["APP_LAUNCHED"])

        # Launch clientside app
        self.currentApp = app
        self.currentApp.setErrorCallback(self.appErrorCallback)
        self.currentApp.start()
        return True

    def stopApp(self):
        if self.currentApp is None:
            raise Exception("No app is currently started")

        # Send payload
        payload = json.dumps({"cmd":"stop"})
        try:
            self.socket.sendall(bytes(payload,"utf-8"))
        except:
            self.notConnectedErrorCallback()
            return False

        # Stop clientside app
        self.currentApp.stop()
        self.currentApp = None
        return True

    def setAppErrorCallback(self,f):
        self.appErrorCallback = f
    def setNotConnectedErrorCallback(self,f):
        self.connected = False
        self.notConnectedErrorCallback = f

    def getServerSettings(self):
        try:
            self.socket.sendall(bytes(json.dumps({"cmd":"getconfig"}),"utf-8"))
        except:
            self.notConnectedErrorCallback()
            return

        recv = self.socket.recv(1024*4)
        if not recv:
            self.notConnectedErrorCallback()
            return
        else:
            return json.loads(recv)


    def setServerSettings(self,data):
        try:
            self.socket.sendall(bytes(json.dumps({"cmd":"setconfig","settings":data}),"utf-8"))
        except:
            self.notConnectedErrorCallback()
            return

        recv = json.loads(self.socket.recv(1024))
        if not recv:
            self.notConnectedErrorCallback()
            return

        if recv["SET_CONFIG_RESP"] is not True:
            raise Exception(recv["SET_CONFIG_RESP"])
