import sys,os
import json

class Config:
    def __init__(self,apps):
        self.__DICT = None
        try:
            with open(self.__getPath__("config.json"),"r") as data:
                self.parseClientSettings(data,file=True)
        except:
            raise Exception("Failed to load %s" % self.__getPath__("config.json"))

        # Load apps configs
        self.appSettings = {}
        for app in apps:
            try:
                filename = self.__getPath__("%s_config.json" % app)
                with open(filename,"r") as data:
                    self.parseAppSettings(app,data,file=True)
            except:
                raise Exception("Failed to load %s" % filename)

    def parseClientSettings(self,data,file=False):
        config = json.load(data) if file else data
        tmp = {}
        tmp["HOST"] = config["HOST"]
        tmp["PRIMARY_PORT"] = config["PRIMARY_PORT"]
        tmp["SECONDARY_PORT"] = config["SECONDARY_PORT"]
        tmp["AUTOMATIC_CONNECT"] = config["AUTOMATIC_CONNECT"]

        for key,value in tmp.items():
            setattr(self,key,value)
        self.__DICT = tmp

        if "SAVE" in config and config["SAVE"] == True:
            with open(self.__getPath__("config.json"),"w") as file:
                json.dump(tmp,file)

    def parseAppSettings(self,app,data,file=False):
        self.appSettings[app] = json.load(data) if file else data
        if "SAVE" in self.appSettings[app] and self.appSettings[app]["SAVE"] == True:
            del self.appSettings[app]["SAVE"]
            filename = self.__getPath__("%s_config.json" % app)
            with open(filename,"w") as file:
                json.dump(self.appSettings[app],file)

    def getClientSettings(self):
        if self.__DICT is not None:
            return self.__DICT
        return {}

    def __getPath__(self,file):
        fullpath = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),"config")
        return os.path.join(fullpath, file)
