from .audiovisualizer import AudioVisualizer
from .lighting import Lighting

class Controller():
    def __init__(self,config,client,view):
        self.config = config
        self.client = client
        self.client.setAppErrorCallback(lambda err: self.appErrorCallback(err))
        self.client.setNotConnectedErrorCallback(lambda : self.notConnectedErrorCallback())
        self.view = view

    def connect(self):
        try:
            clientSettings = self.getClientSettings()
            host = clientSettings["HOST"]
            port = clientSettings["PRIMARY_PORT"]
            self.client.connect(host,port)
            return True
        except Exception as e:
            return str(e)

    def disconnect(self):
        self.client.disconnect()

    def isConnected(self):
        return self.client.isConnected()

    def startApp(self,name):
        app = None
        appSettings = dict(self.config.appSettings[name])
        serverSettings = self.getServerSettings()
        if not serverSettings:
            return False

        if appSettings is None:
            return "%s is not recognized as an app" % name

        try:
            if name == "audio":
                args = dict(appSettings)
                args["HOST"] = self.config.HOST
                args["PORT"] = self.config.SECONDARY_PORT
                args["LED_COUNT"] = serverSettings["LED_COUNT"]
                app = AudioVisualizer(args)
            elif name == "lighting":
                app = Lighting()
        except Exception as e:
            return str(e)

        try:
            args = dict(appSettings)
            args["cmd"] = name
            return self.client.startApp(app,args)
        except Exception as e:
            return str(e)

    def stopApp(self):
        try:
            return self.client.stopApp()
        except Exception as e:
            return str(e)

    def getClientSettings(self):
        return self.config.getClientSettings()

    def setClientSettings(self,data):
        self.config.parseClientSettings(data)

    def getServerSettings(self):
        return self.client.getServerSettings()

    def setServerSettings(self,data):
        self.client.setServerSettings(data)

    def getAppSettings(self,appCode):
        return self.config.appSettings[appCode]

    def setAppSettings(self,appCode,data):
        try:
            self.config.parseAppSettings(appCode,data)
        except Exception as e:
            return str(e)
        return True

    def appErrorCallback(self,err):
        self.stopApp()
        self.view.appErrorCallback(err)

    def notConnectedErrorCallback(self):
        self.view.notConnectedErrorCallback()
