#!/usr/bin/python3

import socket
import json
from time import sleep

from config import Config
from audiovisualizer import AudioVisualizer

apps = ["audio"]
config = Config(apps)
ledApp = None

with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as sock:
    print(config.APP_HOST)
    sock.connect((config.APP_HOST,config.APP_PRIMARY_PORT))

    def launchApp(payload):
        sock.sendall(bytes(payload+"\n","utf-8"))
        recv = sock.recv(1024)
        try:
            resp = json.loads(recv)
            print(resp)
            if resp["APP_LAUNCHED"] is True:
                return True
        except:
            pass
        return False

    payload = json.dumps({"cmd":"music","AUDIO_SAMPLE_RATE":config.AUDIO_SAMPLE_RATE,"AUDIO_SAMPLE_MAX":config.AUDIO_SAMPLE_MAX})
    if launchApp(payload):
        print("launching music")
        ledApp = AudioVisualizer(config.APP_HOST,config.APP_SECONDARY_PORT)
        ledApp.start()
        sleep(10)
    else:
        print("failed")
