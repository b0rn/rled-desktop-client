#!/usr/bin/python3

if __name__ == "__main__":
    from view.ui_mainwindow import Ui_MainWindow
    from PyQt5 import QtWidgets
    import sys
    from backend.config import Config
    from backend.client import Client
    from backend.controller import Controller

    app = QtWidgets.QApplication(sys.argv)
    appsNames = {"audio":"Audio Visualizer","lighting":"Lighting"}
    config = Config(appsNames.keys())
    client = Client()
    view = Ui_MainWindow(appsNames)
    controller = Controller(config,client,view)

    client.setController(controller)
    view.setController(controller)

    try:
        if config.AUTOMATIC_CONNECT is True:
            client.connect(config.HOST,config.PRIMARY_PORT)
    except Exception as e:
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(str(e))
        msg.setInformativeText("Can't connect to %s:%d" % (config.HOST,config.PRIMARY_PORT))
        msg.setWindowTitle("Error")
        msg.exec_()

    view.setupUI()
    view.show()
    sys.exit(app.exec_())
