from PyQt5 import QtWidgets, QtCore
from . import resources_rc

class Ui_AudioSettingsWindow(QtWidgets.QDialog):
    def __init__(self,icons,settings):
        super().__init__()
        self.icons = icons
        self.initialSettings = settings
        self.edits = {}
        self.setupUI(settings)

    def setupUI(self,settings):
        # Create central widget and form layout
        self.centralwidget = QtWidgets.QWidget(self)
        formLayout = QtWidgets.QFormLayout(self.centralwidget)

        def createPushButton(icon):
            button = QtWidgets.QPushButton()
            button.setIcon(icon)
            button.setIconSize(QtCore.QSize(25, 25))
            button.setFlat(True)
            return button

        # Generate labels
        labels = settings
        for label,value in labels.items():
            if label == "COLORS":
                self.edits[label] = []
                for i in range(len(value)):
                    rgb = value[i]
                    layout = QtWidgets.QHBoxLayout()
                    colorButton = QtWidgets.QPushButton()
                    colorButton.setFlat(True)
                    style = "background-color : rgb(%d,%d,%d)" % (rgb[0],rgb[1],rgb[2])
                    colorButton.setAttribute(QtCore.Qt.WA_StyledBackground)
                    colorButton.setStyleSheet(style)
                    colorButton.setObjectName("COLOR_%d" % i)
                    colorButton.clicked.connect(self.changeColor)
                    upButton = createPushButton(self.icons["up"])
                    upButton.setObjectName("COLOR_%d" % i)
                    upButton.clicked.connect(self.upColor)
                    downButton = createPushButton(self.icons["down"])
                    downButton.setObjectName("COLOR_%d" % i)
                    downButton.clicked.connect(self.downColor)
                    deleteButton = createPushButton(self.icons["trash"])
                    deleteButton.setObjectName("COLOR_%d" % i)
                    deleteButton.clicked.connect(self.deleteColor)
                    layout.addWidget(colorButton)
                    layout.addWidget(upButton)
                    layout.addWidget(downButton)
                    layout.addWidget(deleteButton)
                    self.edits[label].append(rgb)
                    formLayout.addRow(QtWidgets.QLabel("COLOR_%d" %i),layout)
                continue
            elif type(value) is type(True):
                self.edits[label] = QtWidgets.QCheckBox()
                if value:
                    self.edits[label].toggle()
            else:
                self.edits[label] = QtWidgets.QLineEdit()
                self.edits[label].setText(str(value))
            formLayout.addRow(QtWidgets.QLabel(label),self.edits[label])

        # Create save to file button
        self.saveToFile = QtWidgets.QCheckBox()
        formLayout.addRow(QtWidgets.QLabel("Save to file"),self.saveToFile)

        # Create add color button
        addColorButton = QtWidgets.QPushButton("Add color")
        addColorButton.clicked.connect(self.addColor)
        formLayout.addRow(addColorButton)

        # Create confirm and cancel buttons
        confirmButton = QtWidgets.QPushButton("Confirm",self.centralwidget)
        confirmButton.clicked.connect(self.accept)
        cancelButton = QtWidgets.QPushButton("Cancel",self.centralwidget)
        cancelButton.clicked.connect(self.reject)
        formLayout.addRow(confirmButton,cancelButton)

        # Finish configuring window
        self.setWindowTitle("RLED - Audio visualizer settings")
        self.setWindowIcon(self.icons["logo"])
        self.setLayout(formLayout)
        self.adjustSize()

        QtCore.QMetaObject.connectSlotsByName(self)

    def getResults(self):
        res = {}
        for k,v in self.edits.items():
            if k == "COLORS":
                res[k] = []
                for color in self.edits[k]:
                    res[k].append(color)
            elif type(self.initialSettings[k]) is type(1.1):
                res[k] = float(v.text())
            elif type(self.initialSettings[k]) is type(1):
                res[k] = int(v.text())
            elif type(self.initialSettings[k]) is type(True):
                res[k] = bool(v.isChecked())
            else:
                res[k] = v.text()
        if self.saveToFile.isChecked():
            res["SAVE"] = True
        return res

    def addColor(self):
        results = self.getResults()
        results["COLORS"].append([255,255,255])
        layout = self.layout()
        # QtCore.QObjectCleanupHandler().add(layout)
        QtWidgets.QWidget().setLayout(self.layout())
        self.setupUI(results)

    def deleteColor(self):
        sender = self.sender()
        index = int(sender.objectName()[sender.objectName().find("_")+1:])
        results = self.getResults()
        del results["COLORS"][index]
        QtWidgets.QWidget().setLayout(self.layout())
        self.setupUI(results)

    def changeColor(self):
        sender = self.sender()
        index = int(sender.objectName()[sender.objectName().find("_")+1:])
        r,g,b,a = QtWidgets.QColorDialog.getColor().getRgb()
        sender.setStyleSheet("background-color : rgb(%d,%d,%d)" % (r,g,b))
        self.edits["COLORS"][index] = [r,g,b]

    def upColor(self):
        sender = self.sender()
        index = int(sender.objectName()[sender.objectName().find("_")+1:])
        if index - 1 < 0:
            return
        results = self.getResults()
        tmp = results["COLORS"][index]
        results["COLORS"][index] = results["COLORS"][index-1]
        results["COLORS"][index-1] = tmp
        QtWidgets.QWidget().setLayout(self.layout())
        self.setupUI(results)

    def downColor(self):
        sender = self.sender()
        index = int(sender.objectName()[sender.objectName().find("_")+1:])
        results = self.getResults()
        if index + 1 >= len(results["COLORS"]):
            return
        tmp = results["COLORS"][index]
        results["COLORS"][index] = results["COLORS"][index+1]
        results["COLORS"][index+1] = tmp
        QtWidgets.QWidget().setLayout(self.layout())
        self.setupUI(results)
