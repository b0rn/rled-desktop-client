# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from . import resources_rc
from .ui_serversettingswindow import Ui_ServerSettingsWindow
from .ui_clientsettingswindow import Ui_ClientSettingsWindow
from .ui_audiosettingswindow import Ui_AudioSettingsWindow

class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self,apps):
        super().__init__()
        self.apps = apps
        self.appsStatus = {}
        self.controller = None
        self.currentApp = None

    def setController(self,controller):
        self.controller = controller

    def setupUI(self):
        # Create icons
        def createIcon(name):
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap(":/newPrefix/%s.png" % name), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            return icon

        self.icons = {}
        self.icons["logo"] = createIcon("logo")
        self.icons["play"] = createIcon("play")
        self.icons["stop"] = createIcon("stop")
        self.icons["settings"] = createIcon("settings")
        self.icons["connected"] = createIcon("connected")
        self.icons["disconnected"] = createIcon("disconnected")
        self.icons["raspberry"] = createIcon("raspberry")
        self.icons["up"] = createIcon("up")
        self.icons["down"] = createIcon("down")
        self.icons["add"] = createIcon("add")
        self.icons["trash"] = createIcon("trash")

        # Create main window
        self.setObjectName("MainWindow")
        self.resize(800, 600)
        self.setWindowIcon(self.icons["logo"])
        self.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.setUnifiedTitleAndToolBarOnMac(False)
        self.setWindowTitle("RLED")

        # Create central widget & layout
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")

        # Create logo
        self.logo = QtWidgets.QLabel(self.centralwidget)
        self.logo.setText("")
        self.logo.setPixmap(QtGui.QPixmap(":/newPrefix/logo.png"))
        self.logo.setAlignment(QtCore.Qt.AlignCenter)
        self.logo.setObjectName("logo")
        self.verticalLayout_2.addWidget(self.logo)

        # Create spacer
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)

        # Create app name
        self.appVisual = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Pagul")
        font.setPointSize(28)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.appVisual.setFont(font)
        self.appVisual.setAlignment(QtCore.Qt.AlignCenter)
        self.appVisual.setObjectName("appVisual")
        self.verticalLayout_2.addWidget(self.appVisual)

        # Create layout for buttons
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")

        # Create horizontal spacer 1
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)

        # Create app status
        self.appStatus = self.createControlButton(self.centralwidget,self.icons["stop"])
        self.appStatus.hide()
        self.horizontalLayout.addWidget(self.appStatus)

        # Create app settings
        self.appSettings = self.createControlButton(self.centralwidget,self.icons["settings"])
        self.appSettings.hide()
        self.horizontalLayout.addWidget(self.appSettings)

        # Create horizontal spacer 2
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)

        # Set horizontal layout properties and add it to main layout
        self.horizontalLayout.setStretch(0, 5)
        self.horizontalLayout.setStretch(3, 5)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        # Create vertical spacer 2
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem3)

        # Create more apps
        self.moreApps = QtWidgets.QPushButton(self.centralwidget)
        self.moreApps.setObjectName("moreApps")
        self.moreApps.setText("More apps")
        self.verticalLayout_2.addWidget(self.moreApps)

        # Create scroll area
        self.scrollArea = QtWidgets.QScrollArea(self.centralwidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 780, 80))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.scrollArea.setVisible(False)

        # Create scroll area vertical layout
        self.scrollAreaVerticalLayout = QtWidgets.QVBoxLayout(self.scrollArea)
        self.scrollAreaVerticalLayout.setObjectName("scrollAreaVerticalLayout")

        # Add apps
        for appCode,appName in self.apps.items():
            layout = QtWidgets.QHBoxLayout()
            label = QtWidgets.QLabel()
            label.setText(appName)
            icon = self.icons["play"]
            status = self.createControlButton(self.centralwidget,icon,25,25)
            self.appsStatus[appCode] = status
            settings = self.createControlButton(self.centralwidget,self.icons["settings"],25,25)
            layout.addWidget(label)
            layout.addWidget(status)
            layout.addWidget(settings)
            self.scrollAreaVerticalLayout.addLayout(layout)
            # Create names and connections
            status.setObjectName("play_%s" % appCode)
            status.clicked.connect(self.statusClicked)
            settings.setObjectName("settings_%s" % appCode)
            settings.clicked.connect(self.settingsClicked)

        self.verticalLayout_2.addWidget(self.scrollArea)

        # Create toolbar
        toolbar = self.addToolBar("Toolbar")
        # Create connect/disconnect action
        icon = self.icons["connected"] if self.controller.isConnected() else self.icons["disconnected"]
        text = "Disconnect" if self.controller.isConnected() else "Connect"
        self.connectionAction = QtWidgets.QAction(icon,text,self)
        self.connectionAction.triggered.connect(self.connectionClicked)
        toolbar.addAction(self.connectionAction)
        # Create client settings action
        clientSettings = QtWidgets.QAction(self.icons["settings"],"Client settings",self)
        clientSettings.triggered.connect(self.clientSettingsClicked)
        toolbar.addAction(clientSettings)
        # Create server settings
        serverSettings = QtWidgets.QAction(self.icons["raspberry"],"Server settings",self)
        serverSettings.triggered.connect(self.serverSettingsClicked)
        toolbar.addAction(serverSettings)

        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.setCentralWidget(self.centralwidget)
        self.retranslateUi()

        # Create connections (signals/slots)
        self.moreApps.clicked.connect(self.moreAppsClicked)
        self.appStatus.clicked.connect(self.statusClicked)
        self.appSettings.clicked.connect(self.settingsClicked)

        QtCore.QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate

    def createControlButton(self,parent,icon,width=50,height=50):
        button = QtWidgets.QPushButton(parent)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(button.sizePolicy().hasHeightForWidth())
        button.setSizePolicy(sizePolicy)
        button.setFocusPolicy(QtCore.Qt.NoFocus)
        button.setText("")
        button.setIcon(icon)
        button.setIconSize(QtCore.QSize(width, height))
        button.setAutoDefault(False)
        button.setFlat(True)
        return button

    def connectionClicked(self,action):
        if self.controller.isConnected():
            self.controller.disconnect()
            self.connectionAction.setText("Connect")
            self.connectionAction.setIcon(self.icons["disconnected"])
        else:
            connect = self.controller.connect()
            if type(connect) is type("test"):
                self.displayError(connect)
            else:
                self.connectionAction.setText("Disconnect")
                self.connectionAction.setIcon(self.icons["connected"])

    def clientSettingsClicked(self,action):
        if self.controller is None:
            return
        clientSettingsDialog = Ui_ClientSettingsWindow(self.icons,self.controller.getClientSettings())
        if clientSettingsDialog.exec_():
            newSettings = clientSettingsDialog.getResults()
            self.controller.setClientSettings(newSettings)

    def serverSettingsClicked(self,action):
        serverSettings = self.controller.getServerSettings()
        if not serverSettings:
            return

        serverSettingsDialog = Ui_ServerSettingsWindow(self.icons,serverSettings)
        if serverSettingsDialog.exec_():
            newSettings = serverSettingsDialog.getResults()
            self.controller.setServerSettings(newSettings)

    def moreAppsClicked(self):
        self.scrollArea.setVisible(not self.scrollArea.isVisible())
        self.moreApps.setText("Less apps" if self.scrollArea.isVisible() else "More apps")

    def statusClicked(self):
        if self.controller is None:
            return
        sender = self.sender()
        status = sender.objectName()[:sender.objectName().find("_")]
        appCode = sender.objectName()[sender.objectName().find("_")+1:]
        appName = self.apps[appCode]

        if status == "play":
            appStartedResult = self.controller.startApp(appCode)
            if appStartedResult is True:
                self.setCurrentApp(appCode,appName)
            elif type(appStartedResult) is type("test"):
                self.displayError(appStartedResult)
        elif self.currentApp is not None:
            appStoppedResult = self.controller.stopApp()
            if type(appStoppedResult) is type("test"):
                self.displayError(appStoppedResult)
            self.setAppToStopped()

    def settingsClicked(self,value):
        if self.controller is None:
            return
        sender = self.sender()
        appCode = sender.objectName()[sender.objectName().find("_")+1:]
        appName = self.apps[appCode]
        appSettings = self.controller.getAppSettings(appCode)
        appSettingsDialog = None
        if appCode == "audio":
            appSettingsDialog = Ui_AudioSettingsWindow(self.icons,appSettings)
        else:
            self.displayError("Could not find UI for %s" % appName)
        if appSettingsDialog.exec_():
            newSettings = appSettingsDialog.getResults()
            self.controller.setAppSettings(appCode,newSettings)

    def setCurrentApp(self,appCode,appName):
        self.appVisual.setText(appName)
        self.appStatus.setIcon(self.icons["stop"])
        self.appStatus.setObjectName("stop_%s" % appCode)
        self.appStatus.show()
        self.appSettings.show()
        self.appSettings.setObjectName("currentSettings_%s" % appCode)
        self.appsStatus[appCode].setIcon(self.icons["stop"])
        self.appsStatus[appCode].setObjectName("stop_%s" % appCode)
        self.currentApp = appCode

    def setAppToStopped(self):
        if self.currentApp is None:
            return
        self.appStatus.setIcon(self.icons["play"])
        self.appStatus.setObjectName("play_%s" % self.currentApp)
        self.appsStatus[self.currentApp].setIcon(self.icons["play"])
        self.appsStatus[self.currentApp].setObjectName("play_%s" % self.currentApp)

    def notConnectedErrorCallback(self):
        self.displayError("Not connected to server !")
        self.connectionAction.setIcon(self.icons["disconnected"])
        self.connectionAction.setText("Connect")

    def appErrorCallback(self,err):
        self.displayError(err)

    def displayError(self,err):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(str(err))
        msg.setWindowTitle("Error")
        msg.exec_()
