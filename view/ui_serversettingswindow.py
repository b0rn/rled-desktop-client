# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets, QtCore
from . import resources_rc

class Ui_ServerSettingsWindow(QtWidgets.QDialog):
    def __init__(self,icons,settings):
        super().__init__()
        self.initialSettings = settings
        self.edits = {}
        self.setupUI(icons,settings)

    def setupUI(self,icons,settings):
        # Create central widget and form layout
        self.centralwidget = QtWidgets.QWidget(self)
        formLayout = QtWidgets.QFormLayout(self.centralwidget)

        # Generate labels
        labels = settings
        for label,value in labels.items():
            if label == "LED_COUNT":
                self.edits[label] = QtWidgets.QSpinBox()
                self.edits[label].setMinimum(0)
                self.edits[label].setMaximum(9999)
                self.edits[label].setValue(value)
            elif label == "LED_BRIGHTNESS":
                layout = QtWidgets.QHBoxLayout()
                fieldLabel = QtWidgets.QLabel(label)
                slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
                slider.setMinimum(0)
                slider.setMaximum(100)
                slider.setValue(int(value*100))
                percentLabel = QtWidgets.QLabel("%d%%" % int(value*100))
                slider.valueChanged.connect(lambda v : percentLabel.setText("%d%%" % v))
                layout.addWidget(fieldLabel)
                layout.addWidget(slider)
                layout.addWidget(percentLabel)
                self.edits[label] = slider
                formLayout.addRow(layout)
                continue
            elif type(value) is type(True):
                self.edits[label] = QtWidgets.QCheckBox()
                if value:
                    self.edits[label].toggle()
            else:
                self.edits[label] = QtWidgets.QLineEdit()
                self.edits[label].setText(str(value))
            formLayout.addRow(QtWidgets.QLabel(label),self.edits[label])

        # Create save to file button
        self.saveToFile = QtWidgets.QCheckBox()
        formLayout.addRow(QtWidgets.QLabel("Save to file"),self.saveToFile)

        # Create confirm and cancel buttons
        confirmButton = QtWidgets.QPushButton("Confirm",self.centralwidget)
        confirmButton.clicked.connect(self.accept)
        cancelButton = QtWidgets.QPushButton("Cancel",self.centralwidget)
        cancelButton.clicked.connect(self.reject)
        formLayout.addRow(confirmButton,cancelButton)

        # Finish configuring window
        self.setWindowTitle("RLED - Server settings")
        self.setWindowIcon(icons["logo"])
        self.setLayout(formLayout)

        QtCore.QMetaObject.connectSlotsByName(self)

    def getResults(self):
        res = {}
        for k,v in self.initialSettings.items():
            input = self.edits[k]
            value = None
            if k == "LED_COUNT":
                value = int(input.value())
            elif k == "LED_BRIGHTNESS":
                value = float(input.value()/100.0)
            elif type(v) is type(1.1):
                value = float(input.text())
            elif type(v) is type(1):
                value = int(input.text())
            elif type(v) is type(True):
                value = bool(input.isChecked())
            else:
                value = input.text()
            res[k] = value
        if self.saveToFile.isChecked():
            res["SAVE"] = True
        return res
